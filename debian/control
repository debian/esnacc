Source: esnacc
Maintainer: Debian QA Group <packages@qa.debian.org>
Section: devel
Priority: optional
Build-Depends: autotools-dev,
               libtool,
               libfl-dev,
               debhelper (>= 9),
               dh-autoreconf,
               bison,
               flex,
               docbook-xsl,
               unzip,
               xsltproc
Build-Depends-Indep: libreoffice-writer-nogui
Rules-Requires-Root: no
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/debian/esnacc
Vcs-Git: https://salsa.debian.org/debian/esnacc.git
Homepage: http://esnacc.org

Package: esnacc
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: libesnacc-dev,
            esnacc-doc
Suggests: automake
Description: ASN.1 to C or C++ or IDL compiler
 esnacc is short for "Enhanced Sample Neufeld ASN.1 to C Compiler" and ASN.1
 stands for Abstract Syntax Notation One (ITU-T X.208/ISO 8824).
 esnacc supports a subset of ASN.1 1988.
 .
 Given an ASN.1 source file(s) esnacc can produce:
 .
 1. C routines for BER encoding, decoding, printing and freeing.
 2. C++ routines for BER encoding, decoding, and printing.
 3. A type table that can be used with C driver routines
    for BER encoding, decoding, printing and freeing.
 .
 If you want to build esnacc based applications, you want to install
 the libesnacc-dev package, too.  Your application will then depend on
 the esnacc libraries, you find in the libesnacc180 package.

Package: libesnacc-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends},
         libesnacc180 (= ${binary:Version})
Recommends: esnacc-doc
Conflicts: libsnacc-dev
Replaces: libsnacc-dev
Multi-Arch: same
Description: ASN.1 to C or C++ or IDL compiler, development files
 esnacc is short for "Enhanced Sample Neufeld ASN.1 to C Compiler" and ASN.1
 stands for Abstract Syntax Notation One (ITU-T X.208/ISO 8824).
 Snacc supports a subset of ASN.1 1988.
 .
 This package contains the static libraries and C/C++ header files for
 snacc development.

Package: libesnacc180
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Conflicts: libsnacc0
Replaces: libsnacc0
Description: ASN.1 to C or C++ or IDL compiler, shared libraries
 esnacc is short for "Enhanced Sample Neufeld ASN.1 to C Compiler" and ASN.1
 stands for Abstract Syntax Notation One (ITU-T X.208/ISO 8824).
 Snacc supports a subset of ASN.1 1988.
 .
 These are the shared libraries for programs developed using snacc.

Package: esnacc-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: ASN.1 to C or C++ or IDL compiler, documentation
 esnacc is short for "Enhanced Sample Neufeld ASN.1 to C Compiler" and ASN.1
 stands for Abstract Syntax Notation One (ITU-T X.208/ISO 8824).
 Snacc supports a subset of ASN.1 1988.
 .
 The documentation for snacc in PDF format.
